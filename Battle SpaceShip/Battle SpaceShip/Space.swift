//
//  Space.swift
//  Battle SpaceShip
//
//  Created by Allison Lindner on 19/05/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

import Foundation
import SpriteKit

class Space {
	
	var players = Array<SpaceShip>(count: 0, repeatedValue: SpaceShip())				//Array com as naves dos jogadores
	var size: (width: Int, height: Int)		//Tamanho em tiles
	var tiles: Array<Array<SKShapeNode>>!
	var tileSize: CGFloat
	
	init (size: (x: Int, y: Int), tileSize: CGFloat) {
		self.tileSize = tileSize
		self.size.width = size.x
		self.size.height = size.y
	}
	
	func moveSpaceShip(spaceShip: SpaceShip , toXY: [(x: Int, y: Int)]) {
		if spaceShip.energy > 0 {
			for xy in toXY {
				if abs(xy.x + xy.y) == 2 {
					if spaceShip.energy >= 2 {
						spaceShip.position.x += xy.x
						spaceShip.position.y += xy.y
						spaceShip.energy -= 2
					} else {
						break
					}
				} else if abs(xy.x + xy.y) == 1 {
					if xy.x == 1 && xy.y == 0 {
						spaceShip.position.x += xy.x
						spaceShip.energy -= 1
					} else if xy.y == 1 && xy.x == 0 {
						spaceShip.position.y += xy.y
						spaceShip.energy -= 1
					} else {
						break
					}
				}
				
				if spaceShip.energy == 0 {
					break
				}
			}
		}
	}
	
	func render(scene: SKScene) {
		self.tiles = Array<Array<SKShapeNode>>(count: self.size.height, repeatedValue: Array<SKShapeNode>())
		for var lin = 1; lin <= self.size.height; lin++ {
			self.tiles.append(Array<SKShapeNode>(count: self.size.width, repeatedValue: SKShapeNode()))
			for var col = 1; col <= self.size.width; col++ {
				var boundingPoint = SKShapeNode(rectOfSize: CGSize(width: self.tileSize, height: self.tileSize))
				boundingPoint.position = CGPointMake(CGFloat(Int(CGFloat(col) * self.tileSize)), CGFloat(Int(CGFloat(scene.size.height) - CGFloat(lin) * tileSize)))
				boundingPoint.hidden = true
				boundingPoint.name = "boundingPoint"
				boundingPoint.strokeColor = UIColor.clearColor()
				
				var point = SKShapeNode(circleOfRadius: 0.5)
				point.position = CGPointZero
				point.name = "point"
				point.fillColor = UIColor.grayColor()
				point.strokeColor = UIColor.grayColor()
				
				boundingPoint.addChild(point)
				scene.addChild(boundingPoint)
				self.tiles[lin - 1].insert(boundingPoint, atIndex: col - 1)
			}
		}
	}
	
	func renderPlayers() {
		for player in self.players {
			var tile = self.tiles[player.position.x][player.position.y] as SKShapeNode
			tile.name = "player"
			tile.fillColor = UIColor.whiteColor()
			
			if let point = tile.childNodeWithName("point") {
				var pointMutable = point as! SKShapeNode
				pointMutable.fillColor = UIColor.whiteColor()
				pointMutable.strokeColor = UIColor.whiteColor()
			}
			
			for var lin = -player.fov; lin <= player.fov; lin++ {
				for var col = -player.fov; col <= player.fov; col++ {
					if player.position.x + col >= 0 && player.position.y + lin >= 0 {
						var fovTile = self.tiles[player.position.x + col][player.position.y + lin]
						
						let playerX = CGFloat(player.position.x) * tileSize
						let playerY = CGFloat(player.position.y) * tileSize
						
						println(CGFloat(player.position.x + col) * tileSize)
						println(CGFloat(player.position.y + lin) * tileSize)
						
						let powX = pow((CGFloat(player.position.x + col) * tileSize) - playerX, 2)
						let powY = pow((CGFloat(player.position.y + lin) * tileSize) - playerY, 2)
						
						let distance = sqrt(powX + powY)
						
						if distance <= (CGFloat(player.fov) * tileSize) {
							fovTile.hidden = false
						} else if distance <= (CGFloat(player.fov) * tileSize) {
							fovTile.hidden = false
						}
					}
				}
			}
		}
	}
	
	func addPlayer(player: SpaceShip) {
		self.players.append(player)
	}
}