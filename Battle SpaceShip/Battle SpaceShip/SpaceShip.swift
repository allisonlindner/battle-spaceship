//
//  SpaceShip.swift
//  Battle SpaceShip
//
//  Created by Allison Lindner on 19/05/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

import Foundation

class SpaceShip {
	
	var position: (x: Int, y: Int) = (x: 0, y: 0)
	var type: Int = 0
	var engine: Int = 1
	var energy: Int = 1
	var fov: Int = 3
	
	init () {
		
	}
}