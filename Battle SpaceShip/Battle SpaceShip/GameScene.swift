//
//  GameScene.swift
//  Battle SpaceShip
//
//  Created by Allison Lindner on 19/05/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
	
	var tileSize: CGFloat = 25
	var space: Space!
	
	required init?(coder aDecoder: NSCoder) {
	    fatalError("init(coder:) has not been implemented")
	}
	
	override init(size: CGSize) {
		super.init(size: size)
		space = Space(size: (x: Int(self.size.width / self.tileSize) - 1, y: Int(self.size.height / self.tileSize) - 4), tileSize: tileSize)
		
		var player1 = SpaceShip()
		player1.position.x = 0
		player1.position.y = 0
		player1.fov = 4
		space.addPlayer(player1)
		
		var player2 = SpaceShip()
		player2.position.x = 10
		player2.position.y = 5
		player2.fov = 4
		space.addPlayer(player2)
	}
	
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
		space.render(self)
		space.renderPlayers()
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
		
        for touch in (touches as! Set<UITouch>) {
            let location = touch.locationInNode(self)
			
			let node = self.nodeAtPoint(location)
			
			if node.name == "boundingPoint" {
				if let point = node.childNodeWithName("point") {
					var pointMutable = point as! SKShapeNode
					
					if let pointClicked = pointMutable.childNodeWithName("pointClicked") {
						pointMutable.removeAllChildren()
						
						pointMutable.strokeColor = UIColor.grayColor()
						pointMutable.fillColor = UIColor.grayColor()
					} else {
						var pointClicked = SKShapeNode(circleOfRadius: 5)
						pointClicked.name = "pointClicked"
						pointClicked.strokeColor = UIColor.whiteColor()
						pointClicked.fillColor = UIColor.blueColor()
						
						pointMutable.strokeColor = UIColor.whiteColor()
						pointMutable.fillColor = UIColor.whiteColor()
						
						pointMutable.addChild(pointClicked)
					}
				}
			}
		}
    }
	
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}
